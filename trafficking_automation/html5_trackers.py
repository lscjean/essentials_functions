import xlwings as xw
import os
from pprint import pprint
import datetime as dt
 
 
def main():
    ########################
    ## Defining workspace ##
    ########################
 
    wkb = xw.Book('/Users/lissacj/Documents/PROJECTS/Trafficking_Automation/essent_befr_dal2_terrasje_mediascale_display.xlsx')
    sht = wkb.sheets('Import')
    startRange = 'A12'
    endRange = 'P'+str(sht.range('A12').end('down').last_cell.row)
    rng = sht.range(startRange,endRange).value ## range for data manipulation
    ADV = str(sht.range('B5').value)
    adv = ADV.lower().replace(' ','_').rsplit('_',1)[0].replace('_','') ## capturing Advertiser Name + LowerCase + Underscores
    CMP = str(sht.range('B6').value)
    cmp = CMP.lower().split('_')[1]+'2019' ## capturing Campaign Name + LowerCase + Underscores
    lng = CMP.lower().split('_',)[2].split(' ')[0]
    LandingPage = input('Please, specify your Landing Page : ')
    print("Thank you ! Youre Landing page is now \"",LandingPage,"\"")
    print("Processing... \n")   
    print('Workspace is now defined')
 
    ###############################
    ## END of Defining Workspace ##
    ###############################
 
    ######################
    ## Preparing Export ##
    ######################
 
    display = ''
    if 'display' in [x.name for x in wkb.sheets]:
        display = wkb.sheets('display')
    else:
        wkb.sheets.add('display')
        display = wkb.sheets('display')
    display.range('A1').value = ['Name (required)','Description','Width  (required)','Height  (required)','AdTag  (required)','Landing Page URLs  (required)','Ad Server','Creative Placement ID','Yahoo Offer Type','Is Securable','Flight Start Date','Flight End Date','Flight Time Zone ID','Is Expandable','Expansion Direction','Expansion Action','Expansion Vendor','MRAID Version','Third Party Tracking Tag 1','Third Party Tracking Tag 2','Third Party Tracking Tag 3','Third Party Tracking URL 1','Third Party Tracking URL 2','Third Party Tracking URL 3']
    print('Export destination defined')
 
    #############################
    ## END of preparing Export ##
    #############################
 
    #######################
    ## Naming Convention ##
    #######################
    DisnameIn = []
    DisnameOut = []
 
    for row in rng:
        if row[0]!='1' and row[0]!='0':
            DisnameIn.append('_'.join([row[4],adv,lng,cmp,(row[3].lower().replace('-','_',1))]))
        else:
            pass
    for elem in DisnameIn:
        DisnameOut.append([elem])
    display.range('A2').value = DisnameOut
    ## END Display ##
 
    ###########################
    ## END Naming Convention ##
    ###########################
     
    ###########################################
    ## Inserting Macros for Clicks & Display ##
    ###########################################
 
    DismacroIn =[]
    DismacroOut= []
 
    ## Display ##  
    for row in rng:
        if row[0]!='1':
            DismacroIn.append(str(row[11])\
            .replace('<a href="','<a href=\"%%TTD_CLK%%')\
            .replace('var ftClick = \"\"','var ftClick =\"%%TTD_CLK%%\"')\
            .replace('var ftCustom = \"\";','var ftCustom = \"%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%_%%TTD_DEVICEID%%\";'))
        else:
            pass
    for elem in DismacroIn:
        DismacroOut.append([elem])
 
    display.range('E2').value = DismacroOut
    ## END Display ##
     
    ##################################################
    ## End of Inserting Macros for Clicks & Display ##
    ##################################################
     
    #########################
    ## Display Spreadsheet ##
    #########################
 
    widthIn=[]
    widthOut=[]
    heightIn=[]
    heightOut=[]
    placeIDin=[]
    placeIDout=[]
     
    for row in rng:
        if row[0]!='1':
            widthIn.append(row[0]) ## Width
        else:
            pass
 
    for elem in widthIn:
        widthOut.append([elem])
 
    display.range('C2').value = widthOut
 
    for row in rng:
        if row[0]!='1':
            heightIn.append(row[1]) ## height
        else:
            pass
 
    for elem in heightIn:
        heightOut.append([elem])
 
    display.range('D2').value = heightOut
 
    LPrng = 'F'+str(display.range('A2').end('down').last_cell.row)
    display.range('F2',LPrng).value = LandingPage
 
    FTrng = 'G'+str(display.range('A2').end('down').last_cell.row)
    display.range('G2',FTrng).value = "Flashtalking"
 
    for row in rng:
        if row[0]!='1':
            placeIDin.append(row[4]) ## Placement ID
        else:
            pass
 
    for elem in placeIDin:
        placeIDout.append([elem])
     
    display.range('H2').value = placeIDout
 
    SecuRng = 'J'+str(display.range('A2').end('down').last_cell.row)
    display.range('J2',SecuRng).value = "Yes"
 
    #############################
    ## END Display Spreadsheet ##
    #############################
     
    print('\nTroubleshooting Phase ... please wait')
    print('Loading Action logs ...')
 
     
    #####################
    ## TroubleShooting ##
    #####################
     
    right = (95, 221, 97)
    wrong = (209, 31, 31)
 
    # Display #
     
    displayLastRow = 'T'+str(display.range('A2').end('down').last_cell.row)
    displayRng = display.range('A2',displayLastRow).value
     
    print('\nDISPLAY PLACEMENT ID ASSIGNEMENT\n')
 
    # Placement Id Assigment
    rowCounter = 2
    for row in displayRng:
        if str(int(row[7]))==row[0].split('_',1)[0]:
            print("True - ",row[0]," has matching clicktracker ID (",int(row[7]),")")
            display.range((rowCounter,8)).color = right
        else:
            print("False - ",row[0]," has NOT matching clicktracker ID (",int(row[7]),")")
            display.range((rowCounter,8)).color = wrong
        rowCounter = rowCounter+1
 
    print('\nDISPLAY TAG ASSIGNEMENT\n')
 
    # Tag Assigment
    rowCounter = 2
    for row in displayRng:
        if (str(int(row[7]))in row [4]) and ("https:" in row[4]):
            print("True - ",row[0]," has the right HTTPS Tag with ID (",int(row[7]),")")
            display.range((rowCounter,5)).color = right
        elif str(int(row[7]))in row [4]:
            print("False - ",row[0]," has NOT the right Tag ID (",int(row[7]),")")
            display.range((rowCounter,5)).color = wrong
        elif ("https:" in row[4]):
            print("False - ",row[0]," has NOT HTTPS Tag ID (",int(row[7]),")")
            display.range((rowCounter,5)).color = wrong
        rowCounter = rowCounter+1
     
    print('\nSECURITY ASSIGNEMENT\n')
 
    # is securable
    rowCounter = 2
    for row in displayRng:
        if row[9]=="Yes":
            print("True - Your placement ID :",int(row[7]),"is set to securable (",row[9],")")
            display.range((rowCounter,10)).color = right
        else:
            print("False - Your placement ID :",int(row[7]),"is NOT set to securable (",row[9],")")
            display.range((rowCounter,10)).color = wrong
        rowCounter = rowCounter+1
 
    ## Wrap Text ##
    display.range('A:ZZ').api.wrap_text.set(False)
     
 
    ## End of Wrap Text ##
     
    print('\n8==============================================================D\'\'')
    print('\nYOUR CODE HAS SUCCESSFULY RUN\n')
    print("Congrats !")
    print("\nGG\n")
 
print("\nHello There !")
print("Welcome to the machine, General Kenobi\n")  
 
if __name__ == '__main__':
    main()