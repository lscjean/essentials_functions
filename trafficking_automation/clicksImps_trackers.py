import xlwings as xw
import os
from pprint import pprint
import datetime as dt
 
 
def main():
    ########################
    ## Defining workspace ##
    ########################
 
    wkb = xw.Book('/Users/lissacj/Documents/PROJECTS/Trafficking_Automation/essent_befr_dal2_mediascale_display_v2.xlsx')
    sht = wkb.sheets('Import')
 
    workspace = len(sht.range('A11','A'+str(sht.range('A11').end('down').last_cell.row)))
    if workspace == 2:
        rng = sht.range('A12','J12').value
    else:
        rng = sht.range('A12','J'+str(sht.range('A12').end('down').last_cell.row)).value
 
    startRange = 'A12'
    endRange = 'J'+str(sht.range('A12').end('down').last_cell.row)
    #rng = sht.range(startRange,endRange).value ## range for data manipulation
    ADV = str(sht.range('B5').value)
    adv = ADV.lower().replace(' ','_').rsplit('_',1)[0].replace('_','') ## capturing Advertiser Name + LowerCase + Underscores
    CMP = str(sht.range('B6').value)
    site = str(sht.range('B3').value).lower().split(' ')[0]
    cmp = CMP.lower().split('_')[1].split(' ')[0] ## capturing Campaign Name + LowerCase + Underscores
    lng = CMP.lower().split('_',)[2].split(' ')[0]
 
    sht.range('K11').value = 'Impressions'
    sht.range('K11',('K'+str(sht.range('A11').end('down').last_cell.row))).color = (214, 214, 214)
    sht.range('L11').value = 'Static Click'
    sht.range('M11').value = 'Update Click'
    sht.range('M11',('M'+str(sht.range('A11').end('down').last_cell.row))).color = (214, 214, 214)
 
    print("Processing... \n")   
    print('Workspace is now defined')
 
    ###############################
    ## END of Defining Workspace ##
    ###############################
 
    #######################
    ## Naming Convention ##
    #######################
 
    nameIn = []
    nameOut = []
    placement_ID = sht.range('E12').value
    placement_name = sht.range('G12').value.lower().replace('-','_',1)
    underscore = '_'
 
    if workspace == 2:
        sht.range('F12').value = underscore.join([placement_ID,adv,lng,cmp,site,(placement_name.lower().replace('-','_',1))])
    else:
        for row in rng:
            nameIn.append('_'.join([row[4],adv,lng,cmp,site,(row[3].lower().replace('-','_',1))]))
             
        for elem in nameIn:
            nameOut.append([elem])
     
        sht.range('F12').value = nameOut
        sht.range('F11').value = "Naming Convention"
        sht.range('f:f').autofit()
 
    ###########################
    ## END Naming Convention ##
    ###########################
 
    #########################
    ## Impression Trackers ##
    #########################
 
    impIn = []
    impOut = []
 
    if workspace == 2 and ('/imp/' in sht.range('J12').value):
        if ('mediascale' in site) or ('teads' in site):
            impression_tracker = sht.range('J12').value\
                                .split('<!-- Normal Click Command Clicktag1 -->')[0]\
                                .split('src="')[1].split('"/>')[0]\
                                .replace('?cachebuster=[CACHEBUSTER]','?ft_custom=%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%&%%TTD_DEVICEID%%&cachebuster=[CACHEBUSTER]&url=https://s3.eu-central-1.amazonaws.com/semasio/main.js')
        else:
            impression_tracker = sht.range('J12').value\
                    .split('<!-- Normal Click Command Clicktag1 -->')[0]\
                    .split('src="')[1].split('"/>')[0]
 
        sht.range('K12').value = impression_tracker
 
    elif workspace != 2:
        for row in rng:
                if '/imp/' in row[9]:
                    if ('mediascale' in site) or ('teads' in site):
                        impIn.append(row[9].split('<!-- Normal Click Command Clicktag1 -->')[0]\
                                         .split('src="')[1].split('"/>')[0]\
                                         .replace('?cachebuster=[CACHEBUSTER]','?ft_custom=%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%&%%TTD_DEVICEID%%&cachebuster=[CACHEBUSTER]&url=https://s3.eu-central-1.amazonaws.com/semasio/main.js'))
                    else:
                        impIn.append(row[9].split('<!-- Normal Click Command Clicktag1 -->')[0].split('src="')[1].split('"/>')[0])
                else:
                    pass
 
        for elem in impIn:
            impOut.append([elem])
 
        sht.range('K12').value = impOut
    else:
        pass
 
    """if workspace == 2:
        sht.range('K12').value = impression_tracker
    else:
        for row in rng:
            impIn.append(row[9].split('<!-- Normal Click Command Clicktag1 -->')[0].split('src="')[1].split('"/>')[0])
         
        for elem in impIn:
            impOut.append([elem])
 
        sht.range('K12').value = impOut"""
 
    #####################
    ## Static Trackers ##
    #####################
 
    StaticClickIn = []
    StaticClickOut = []
    adwInsertion = '&ft_custom={campaignid}|{adgroupid}'
    adwCheck = '?ft_keyword={keyword}&ft_section={matchtype}'
    adwOutput = adwCheck+adwInsertion
    fbCheck = '?ft_keyword=[INSERT_AD]&ft_section=[INSERT_ADGROUP]'
    fbInsertion = '?ft_keyword={{ad.id}}&ft_section={{adset.id}}&ft_custom={{campaign.id}}|{{adset.id}}|{{ad.id}}'
    allCheck = '?ft_width=1&ft_height=1'
    fbInsertion_2 = '?ft_width=1&ft_height=1&ft_keyword={{ad.id}}&ft_section={{adset.id}}&ft_custom={{campaign.id}}|{{adset.id}}|{{ad.id}}'
    fbOutput = adwCheck+fbInsertion
     
    pixel_tracker = sht.range('J12').value
    if workspace == 2:
        if (('adwords' in site) or ('gmail' in site)) and (adwCheck in pixel_tracker):
            StaticClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
            .replace(adwCheck,adwOutput))
 
        elif ('bing' in site) and (adwCheck in pixel_tracker):
            StaticClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
            .replace(adwCheck,'?ft_keyword={keyword:default}&ft_section={matchtype}&ft_custom={CampaignId}|{AdGroupId}|{AdId}|{TargetId}'))
 
        elif (('facebook' in site) or ('instagram' in site)) and ('[INSERT_AD]' in pixel_tracker):
            StaticClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .replace('ft_keyword=[INSERT_AD]&ft_section=[INSERT_ADGROUP]','ft_keyword={{ad.id}}&ft_section={{adset.id}}&ft_custom={{campaign.id}}|{{adset.id}}|{{ad.id}}'))
         
        elif ('mediascale' in site) or ('teads' in site):
            StaticClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
            .replace('?url','?ftCustom=%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%_%%TTD_DEVICEID%%&url'))
 
        elif (('facebook' in site) or ('instagram' in site)):
            StaticClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
            .replace('?url','?ft_keyword={{ad.id}}&ft_section={{adset.id}}&ft_custom={{campaign.id}}|{{adset.id}}|{{ad.id}}&url'))
 
        else:
            StaticClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[0])
     
        sht.range('L12').value = StaticClickIn
 
    else:
        for row in rng:
            if (('adwords' in site) or ('gmail' in site)) and (adwCheck in row[9]):
                StaticClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
                .replace(adwCheck,adwOutput))
 
            if ('bing' in site) and (adwCheck in row[9]):
                StaticClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
                .replace(adwCheck,'?ft_keyword={keyword:default}&ft_section={matchtype}&ft_custom={CampaignId}|{AdGroupId}|{AdId}|{TargetId}'))
 
            elif (('facebook' in site) or ('instagram' in site)) and ('[INSERT_AD]' in row[9]):
                StaticClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .replace('ft_keyword=[INSERT_AD]&ft_section=[INSERT_ADGROUP]','ft_keyword={{ad.id}}&ft_section={{adset.id}}&ft_custom={{campaign.id}}|{{adset.id}}|{{ad.id}}'))
 
            elif ('mediascale' in site) or ('teads' in site):
                StaticClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
                .replace('?url','?ftCustom=%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%_%%TTD_DEVICEID%%&url'))
 
            elif (('facebook' in site) or ('instagram' in site)):
                StaticClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[0]\
                .replace('?url','?ft_keyword={{ad.id}}&ft_section={{adset.id}}&ft_custom={{campaign.id}}|{{adset.id}}|{{ad.id}}&url'))
             
            else:
                StaticClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[0])
 
         
        for elem in StaticClickIn:
            StaticClickOut.append([elem])
 
    sht.range('L12').value = StaticClickOut
 
    #####################
    ## Update Trackers ##
    #####################
 
    UpdateClickIn = []
    UpdateClickOut = []
 
    if workspace == 2:
        if (('facebook' in site) or ('instagram' in site)) and (allCheck in pixel_tracker):
            UpdateClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[1]\
            .replace(allCheck,fbInsertion_2))
         
        elif ('adwords' in site) and (allCheck in pixel_tracker):
            UpdateClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[1]\
            .replace(allCheck,adwOutput))
         
        elif ('mediascale' in site) or ('teads' in site):
            UpdateClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[1]\
            .replace('&url','&ftCustom=%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%_%%TTD_DEVICEID%%&url'))
 
        else:
            UpdateClickIn.append(pixel_tracker.split('<!-- Normal Click Command Clicktag1 -->')[1]\
            .split('<!-- Updateable Click Command Clicktag1 -->')[1])
         
        sht.range('M12').value = UpdateClickIn
 
    elif '<!-- Updateable Click Command Clicktag1 -->' in row[9]:
        for row in rng:
            if (('facebook' in site) or ('instagram' in site)) and (allCheck in row[9]):
                UpdateClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[1]\
                .replace(allCheck,fbInsertion_2))
             
            elif ('adwords' in site) and (allCheck in row[9]):
                UpdateClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[1]\
                .replace(allCheck,adwOutput))
 
            elif ('mediascale' in site) or ('teads' in site):
                UpdateClickIn.append(str(row[9])\
                .split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[1]\
                .replace('&url','&ftCustom=%%TTD_SITE%%_%%TTD_TDID%%_%%TTD_PARTNERID%%_%%TTD_ADVERTISERID%%_%%TTD_CAMPAIGNID%%_%%TTD_ADGROUPID%%_%%TTD_CREATIVEID%%_%%TTD_DEVICEID%%&url'))
 
            else:
                UpdateClickIn.append(str(row[9]).split('<!-- Normal Click Command Clicktag1 -->')[1]\
                .split('<!-- Updateable Click Command Clicktag1 -->')[1])
    else:
        pass
 
    for elem in UpdateClickIn:
        UpdateClickOut.append([elem])
 
    sht.range('M12').value = UpdateClickOut
    ##################
    ## END Trackers ##
    ##################
     
    sht.range('A:ZZ').api.wrap_text.set(False)
    wkb.save()
 
print("\nHello There !")
print("Welcome to the machine, General Kenobi\n")  
 
if __name__ == '__main__':
    main()